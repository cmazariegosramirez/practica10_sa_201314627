const chai = require('chai');
const chaiHttp = require('chai-http');
const express = require('express');
const cors = require('cors');
var request = require('request');
const app = require('../src/routes/catalog')
import { configuration } from '../config/config'

const expect = chai.expect
chai.use(chaiHttp)
chai.should();
app.use(cors());

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

describe('Insert a country with error: ',()=>{
    it('should receive an error', (done) => {
    chai.request(configuration.RECEPTION_API_URL + '/accept')
    .post('/country')
    .send({id:1, country: "Madrid", year: 2010, days: 10})
    .end( function(err,res){
    console.log(res.body)
    expect(res).to.have.status(500);
    done();
    });
    });
   });
   
